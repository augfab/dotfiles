# -*- sh -*-
# vi:ft=sh:

case "${-}" in
  *i*) ;;
  *) return;;
esac

_log () {
    local level
    level="${1:?}"
    shift
    if (( ${#FUNCNAME[@]} >= 2 )); then
       printf 'In function `%s'\'':\n%s: %s: ' \
	      "${FUNCNAME[2]}" \
	      "${BASH_SOURCE[2]}" \
	      "${BASH_LINENO[2]}"
    fi
    printf '%s: %s\n' "${level}" "${*}"
}

_needs_args () {
    local n="${1:-0}"
    shift
    if (( $# != n )); then
	_err "\`${FUNCNAME[1]}' needs exactly ${n} arguments"
    fi
}

_note () {
    _log note "${@}"
}

_warn () {
    _log warning "${@}"
}

_err () {
    _log error "${@}"
    return 1
}

_version_cmp () {
    _needs_args 3 "${@}"
    local op newest
    op="${1}"
    shift
    newest="$(LC_ALL=C printf '%s\n%s\n' "${1}" "${2}" | sort -rV | head -1)"
    case "${op}" in
	eq|\=\=) [[ "${1}" == "${2}" ]];;
	gt|\>)   [[ "${newest}" == "${1}" ]];;
	ge|\>\=) [[ "${newest}" == "${1}" ]] || [[ "${1}" == "${2}" ]];;
	lt|\<)   [[ "${newest}" != "${1}" ]];;
	le|\<\=) [[ "${newest}" != "${1}" ]] || [[ "${1}" == "${2}" ]];;
	*)       _err "Unknown comparison ${op}";;
    esac
}

_available () {
    if (( $# > 0 )); then
	command -v "${1}" >/dev/null 2>&1
    else
	return 1
    fi
}

umask 037

if _available shopt; then
  shopt -s histappend
  shopt -s checkwinsize
  shopt -s cdspell
fi

export HISTIGNORE
export HISTCONTROL=ignoreboth
export PROMPT_COMMAND='history -a'

if _version_cmp ge "${BASH_VERSION}" 4.3; then
  export HISTSIZE=-1
  export HISTFILESIZE=-1
else
  export HISTSIZE=80000
  export HISTFILESIZE=160000
fi

if [[ -d "${HOME}/opt/bin" ]]; then
  export PATH="${HOME}/opt/bin:${PATH}"
fi

PS1='\s${SHLVL+/${SHLVL}} \! \A [$?] \u@\H:\w'
if _available tput; then
    case "${TERM:-}" in
	xterm-*color|rxvt-**color|screen)
	    PS1="$(tput setaf 4)${PS1}$(tput sgr0)"
	    ;;
	linux**)
	    PS1="$(tput setaf 3)${PS1}$(tput sgr0)"
	    ;;
	*)
	    ;;
    esac
fi
PS1="${PS1}"'\n\$ '
export PS1

export LESS=FiQMXr
export TZ=Europe/Paris

if _available nvim; then
    EDITOR=nvim
elif _available vim; then
    EDITOR=vim
else
    EDITOR=vi
fi
export EDITOR

alias ls='ls --color=auto -F'
alias s=ls
alias ll='ls -l'
alias l='ls -al'

alias ..='cd ..'

for f in ~/.dotfiles/bash-completion/*.bash; do
    . "${f}"
done

if [[ -r ~/.bashrc.local ]]; then
    . ~/.bashrc.local
fi
