;;; init --- Initalization for emacs
;;; Commentary:

;;; Code:

(require 'package)
(setq package-archives '(("gnu" . "https://elpa.gnu.org/packages/")
                         ("melpa" . "https://melpa.org/packages/")
			 ("org" . "https://orgmode.org/elpa/")))

(defvar package-list '(
		     async                      ; Needed by helm-core.
		     auto-complete
		     bash-completion
		     beacon
		     bison-mode
		     browse-kill-ring
		     dash
		     elfeed
		     elisp-slime-nav
		     epresent
		     expand-region
		     f                          ; Working with files.
		     flycheck
		     flycheck-package
		     git
		     git-gutter
		     helm
		     helm-core
		     helm-git-grep
		     helm-gtags
		     ini-mode
		     magit
		     magit-popup
		     magit-svn
		     markdown-mode
		     nov
		     org
		     org-plus-contrib
		     ox-reveal
		     package-lint
		     page-break-lines
		     paredit
		     popup
		     rmsbolt
		     s                          ; String manipulation library.
		     ssh-config-mode
		     slime
		     solarized-theme
		     tuareg
		     undo-tree
		     vdiff
		     vlf
		     which-key
		     with-editor
		     yaml-mode
		     ))

(setq load-prefer-newer t)

(package-initialize)

(unless package-archive-contents
  (package-refresh-contents))

(dolist (package package-list)
  (unless (package-installed-p package)
    (package-install package)))


;; Increase garbage collection threshold
(setq gc-cons-threshold 20000000)

;; Disable startup message
(setq inhibit-splash-screen t
      inhibit-startup-message t)
(require 'menu-bar)
(menu-bar-mode -1)
(require 'tool-bar)
(tool-bar-mode -1)
(require 'scroll-bar)
(scroll-bar-mode -1)
(setq scroll-margin 0)

(add-to-list 'default-frame-alist '(fullscreen . maximized))

(global-hl-line-mode 1)

(require 'page-break-lines)
(global-page-break-lines-mode)

(require 'flycheck)
(add-hook 'after-init-hook #'global-flycheck-mode)
(setq flycheck-disabled-checkers '(emacs-lisp-checkdoc)
      flycheck-emacs-lisp-load-path 'inherit)

;; Display available keybindings in popup
(which-key-mode 1)

;; Store backups and auto-saved files in TEMPORARY-FILE-DIRECTORY
;; (/tmp on Unix), instead of in the same directory as the file.
(setq backup-directory-alist
      `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))

;; Ask y/n instead of yes/no
(fset 'yes-or-no-p 'y-or-n-p)

;; Confirm before closing Emacs
(setq confirm-kill-emacs 'y-or-n-p)

;; Human readable units in dired-mode
(setq-default dired-listing-switches "-alh")

;; Set ibuffer name column width
(require 'ibuffer)

(define-ibuffer-column size-h
  (:name "Size" :inline t)
  (cond
   ((> (buffer-size) 1000000) (format "%7.1fM" (/ (buffer-size) 1000000.0)))
   ((> (buffer-size) 1000) (format "%7.1fk" (/ (buffer-size) 1000.0)))
   (t (format "%8d" (buffer-size)))))

(setq ibuffer-formats
      '((mark modified read-only " "
              (name 35 35 :left :nil) " "
              (size-h 9 -1 :right) " "
              (mode 16 16 :left :elide) " "
              filename-and-process)))

;; automatically update dired buffers
(require 'dired)
(setq dired-auto-revert-buffer t)

;; Auto revert files on change
(global-auto-revert-mode t)

;; Enable Narrow To Region Enable narrow-to-region (C-x n n / C-x n
;; w). This is disabled by default to not confuse beginners.
(put 'narrow-to-region 'disabled nil)

;; Windmove is built into Emacs. It lets you move point from window to
;; window using Shift and the arrow keys. This is easier to type than
;; ‘C-x o’ when there are multiple windows open.
(when (fboundp 'windmove-default-keybindings)
  (windmove-default-keybindings))

;; Make windmove work in org-mode when possible.
(add-hook 'org-shiftup-final-hook 'windmove-up)
(add-hook 'org-shiftleft-final-hook 'windmove-left)
(add-hook 'org-shiftdown-final-hook 'windmove-down)
(add-hook 'org-shiftright-final-hook 'windmove-right)

;; Allows to ‘undo’ (and ‘redo’) changes in the window configuration
;; with the key commands ‘C-c left’ and ‘C-c right’.
(when (fboundp 'winner-mode)
  (winner-mode 1))

(global-undo-tree-mode)

(column-number-mode)
(show-paren-mode 1)
(setq line-move-visual t)

(global-set-key (kbd "<f3>") 'compilation-shell-minor-mode)
(global-set-key (kbd "<f4>") 'compile)
(global-set-key (kbd "<f5>") 'gdb)
(global-set-key (kbd "<f6>") 'gdb-restore-windows)
(global-set-key (kbd "<f7>") 'create-tags)
(global-set-key (kbd "<f8>") 'rename-buffer-shell)
(global-set-key (kbd "<f12>") 'shell-clean-exec-last)

(global-set-key (kbd "S-C-<left>") 'shrink-window-horizontally)
(global-set-key (kbd "S-C-<right>") 'enlarge-window-horizontally)
(global-set-key (kbd "S-C-<down>") 'shrink-window)
(global-set-key (kbd "S-C-<up>") 'enlarge-window)
(global-set-key (kbd "C-x g") 'magit-status)
(global-set-key (kbd "C-c t") 'google-translate-at-point)
(global-set-key (kbd "C-c T") 'google-translate-query-translate)
(global-set-key (kbd "C-c f") 'find-name-dired)
(global-set-key (kbd "M-x") 'helm-M-x)
(global-set-key (kbd "C-x b") 'helm-buffers-list)
(global-set-key (kbd "C-x G") 'helm-grep-do-git-grep)
(global-set-key (kbd "C-c SPC") 'ace-jump-mode)
(global-set-key (kbd "M-=") 'er/expand-region)

;; Shortcut for undo redo changes in the window configuration
(global-set-key (kbd "C-c C-<right>") 'winner-redo)
(global-set-key (kbd "C-c C-<left>") 'winner-undo)


;; Shortcut for changing font-size
(define-key global-map (kbd "C-1") 'text-scale-decrease)
(define-key global-map (kbd "C-2") 'text-scale-increase)

;; In magit-mode bind original vc keybindings to magit ones.
(require 'magit-mode)
(define-key magit-mode-map (kbd "C-x v l") 'magit-log-buffer-file)
(define-key magit-mode-map (kbd "C-x v L") 'magit-log-head)

(require 'tramp)
(setq tramp-default-method "scp")

(add-to-list 'auto-mode-alist '("\\.cl$" . c-mode))
(add-to-list 'auto-mode-alist '("\\.def$" . c-mode))
(add-to-list 'auto-mode-alist '("\\.l$" . c-mode))
(add-to-list 'auto-mode-alist '("\\.py\\'" . python-mode))
(add-to-list 'auto-mode-alist '("\\.pyw\\'" . python-mode))
(add-to-list 'auto-mode-alist '("\\.yy$" . bison-mode))
(add-to-list 'auto-mode-alist '("\\.vc" . verilog-mode))
(add-to-list 'auto-mode-alist '("\\.epub\\'" . nov-mode))
(add-to-list 'auto-mode-alist '("\\.service\\'" . ini-mode))
(add-to-list 'auto-mode-alist '("\\.module\\'" . tcl-mode))

;; When Delete Selection mode is enabled, typed text replaces the selection
;; if the selection is active.  Otherwise, typed text is just inserted at
;; point regardless of any selection.
(delete-selection-mode)

(add-to-list 'load-path "~/.emacs.d/lisp/")

;; helm-git-grep conf
(require 'helm-git-grep)
(global-set-key (kbd "C-c G") 'helm-git-grep)
;; Invoke `helm-git-grep' from isearch.
(define-key isearch-mode-map (kbd "C-c G") 'helm-git-grep-from-isearch)
;; Invoke `helm-git-grep' from other helm.
(eval-after-load 'helm
  '(define-key helm-map (kbd "C-c G") 'helm-git-grep-from-helm))

(set-default 'compile-command "make -j")

;; Indentation can insert tabs
(setq-default indent-tabs-mode t)

(require 'git)
(require 'cc-fonts)
(require 'bison-mode)
(require 'auto-complete-config)
(require 'ido)
(require 'helm-config)
(require 'vlf)
(require 'bash-completion)
(require 'whitespace)
(require 'helm-gtags)
(require 'git-gutter)
(require 'browse-kill-ring)
(require 'ox-reveal)
(require 'solarized-theme)
(load-theme 'solarized-light t)

;; explore kill ring history
(setq browse-kill-ring-highlight-inserted-item t
      browse-kill-ring-highlight-current-entry nil
      browse-kill-ring-show-preview t)
(define-key browse-kill-ring-mode-map (kbd "<up>") 'browse-kill-ring-previous)
(define-key browse-kill-ring-mode-map (kbd "<down>") 'browse-kill-ring-forward)

(global-git-gutter-mode t) ;; Show uncommitted git diffs

;; Whenever the window scrolls a light will shine on top of your cursor.
(require 'beacon)
(beacon-mode)
(setq beacon-lighter " ^")

;; Register bash completion for the shell buffer and shell command line.
(bash-completion-setup)

;; auto-complete default configuration
(ac-config-default)

(require 'gdb-mi)
(setq gdb-many-windows t
      gdb-show-main t)

;; Enable prototype help and smart completion
(require 'semantic)
(semantic-mode)
(global-semantic-idle-summary-mode 1)

(add-hook 'before-save-hook 'delete-trailing-whitespace)

;; Ibuffer conf
(defalias 'list-buffers 'ibuffer)
(add-hook 'ibuffer-mode-hook (lambda () (ibuffer-auto-mode 1)))

(require 'ibuf-ext)
(setq ibuffer-saved-filter-groups
      (quote (("default"
	       ("lisp" (or
			(mode . lisp-mode)
			(mode . slime-repl-mode)
			(mode . slime-inspector-mode)
			(name . "^\\*slime-\\(description\\|compilation\\|xref\\)\\*$")
			(name . "^\\*sldb .*\\*$")))
	       ("python" (or
			  (mode . python-mode)
			  (mode . inferior-python-mode)
			  (name . "^\\*Python \\(Check\\|Doc\\)\\*$")))
	       ("shell" (or
			 (mode . shell-mode)
			 (mode . term-mode)
			 (mode . sh-mode)
			 (mode . conf-unix-mode)
			 (mode . eshell-mode)
			 (name . "^\\*Shell Command Output\\*$")))
	       ("C" (or
		     (derived-mode . c-mode)
		     (mode . c++-mode)))
	       ("asm" (mode . asm-mode))
	       ("yaml" (mode . yaml-mode))
	       ("verilog " (mode . verilog-mode))
	       ("dired" (or
			 (mode . dired-mode)
			 (mode . wdired-mode)
			 (mode . archive-mode)
			 (mode . proced-mode)))
	       ("man" (or
		       (mode . Man-mode)
		       (mode . woman-mode)))
	       ("data" (or
			(filename . ".*\\.\\([ct]sv\\|dat\\)$")))
	       ("LaTeX" (or
			 (mode . latex-mode)
			 (mode . tex-shell)
			 (mode . TeX-output-mode)
			 (name . "^\\*\\(Latex Preview Pane \\(Welcome\\|Errors\\)\\|pdflatex-buffer\\)\\*$")))
	       ("text" (mode . text-mode))
	       ("pdf" (or
		       (mode . doc-view-mode)
		       (mode . pdf-view-mode)))
	       ("org" (or (derived-mode . org-mode)
			  (mode . org-agenda-mode)
	                  (mode . org-mode)
			  (filename . "OrgMode")))
	       ("git" (or (derived-mode . magit-mode)
			  (filename . "\\.git\\(ignore\\|attributes\\)$")))
	       ("diff" (or
			(mode . diff-mode)
			(mode . ediff-mode)
			(name . "^\\*[Ee]?[Dd]iff.*\\*$")))
	       ("emacs" (or
			 (mode . emacs-lisp-mode)
			 (mode . lisp-interaction-mode)
			 (mode . help-mode)
			 (mode . Info-mode)
			 (mode . package-menu-mode)
			 (mode . finder-mode)
			 (mode . Custom-mode)
			 (mode . apropos-mode)
			 (mode . ioccur-mode)
			 (mode . occur-mode)
			 (mode . reb-mode)
			 (mode . calc-mode)
			 (mode . calc-trail-mode)
			 (mode . messages-buffer-mode)))
	       ("misc" (name . "^\\*[0-9A-Za-z_]+\\*$"))))))

(add-hook 'ibuffer-mode-hook
	  (lambda ()
	    (ibuffer-switch-to-saved-filter-groups "default")))

(setq ibuffer-show-empty-filter-groups nil)
(setq ibuffer-jump-offer-only-visible-buffers t)

(bash-completion-setup)

(require 'elfeed)
(global-set-key (kbd "C-x w") 'elfeed)
(setq elfeed-feeds '("https://stallman.org/rss/rss.xml"))

(defun cleanup-document ()
  "Examines every character in the document, removing any 'special' characters."
  (interactive)
  (goto-char (point-min))
  (while (not (eobp))
    ;; This regexp considers anything besides tab, newline, and characters
    ;; [space] (ASCII 32) thru tilde (ASCII 126) as 'special'
    (if (looking-at "[\t\n -~]")
        (forward-char 1)
      (delete-char 1 ()))))

(setq whitespace-style '(face lines-tail trailing))
(global-whitespace-mode t)
(global-semantic-stickyfunc-mode t)

(defun create-tags ()
  "Create tags file."
  (interactive)
  (let ((default-directory (read-directory-name "create tags for dir: ")))
    ;; (delete-file "TAGS")
    (shell-command
     "ctags -e -R")
    (visit-tags-table "./")))

(setq c-backslash-max-column 79)

;; C indent
(defun c-lineup-arglist-tabs-only (ignored)
  "Line up argument lists by tabs, not spaces (argument is IGNORED)."
  (let* ((anchor (c-langelem-pos c-syntactic-element))
         (column (c-langelem-2nd-pos c-syntactic-element))
         (offset (- (1+ column) anchor))
         (steps (floor offset c-basic-offset)))
    (* (max steps 1)
       c-basic-offset)))

(add-hook 'c-mode-common-hook
          (lambda ()
            ;; Add kernel style
            (c-add-style
             "linux-tabs-only"
             '("linux" (c-offsets-alist
                        (arglist-cont-nonempty
                         c-lineup-gcc-asm-reg
                         c-lineup-arglist-tabs-only))))
	    (setq show-trailing-whitespace t)))

(add-hook 'c-mode-hook
          (lambda ()
            (setq indent-tabs-mode t)
            (c-set-style "linux-tabs-only")))

(add-hook 'c++-mode-hook
	  (lambda ()
	    (c-set-style "stroustrup")))

(add-hook 'verilog-mode-hook
	  (lambda ()
	    (setq indent-tabs-mode nil)
	    (add-hook 'before-save-hook
		      (lambda ()
			(untabify (point-min) (point-max)))
		      nil t)))

(put 'upcase-region 'disabled nil)

(require 'man)
(setq Man-width 79)
(setq-default fill-column 79)
(setq sentence-end-double-space nil)
(setq colon-double-space nil)

(setq
 helm-gtags-ignore-case t
 helm-gtags-auto-update t
 helm-gtags-use-input-at-cursor t
 helm-gtags-pulse-at-cursor t
 helm-gtags-prefix-key "C-c g"
 helm-gtags-suggested-key-mapping t)

;; Enable helm-gtags-mode
(add-hook 'dired-mode-hook 'helm-gtags-mode)
(add-hook 'eshell-mode-hook 'helm-gtags-mode)
(add-hook 'c-mode-hook 'helm-gtags-mode)
(add-hook 'c++-mode-hook 'helm-gtags-mode)
(add-hook 'asm-mode-hook 'helm-gtags-mode)

(add-hook 'c-mode-common-hook
          (lambda ()
            (when (derived-mode-p 'c-mode 'c++-mode 'java-mode
				  'asm-mode 'dired-mode)
              (helm-gtags-mode))))
(add-hook 'c-mode-common-hook
          (lambda ()
            (when (derived-mode-p 'c-mode 'c++-mode 'java-mode)
              (hs-minor-mode))))

(define-key helm-gtags-mode-map (kbd "C-c g a") 'helm-gtags-tags-in-this-function)
(define-key helm-gtags-mode-map (kbd "C-j") 'helm-gtags-select)
(define-key helm-gtags-mode-map (kbd "M-.") 'helm-gtags-dwim)
(define-key helm-gtags-mode-map (kbd "M-,") 'helm-gtags-pop-stack)
(define-key helm-gtags-mode-map (kbd "C-c <") 'helm-gtags-previous-history)
(define-key helm-gtags-mode-map (kbd "C-c >") 'helm-gtags-next-history)

(defun rename-buffer-shell ()
  "Renames current shell buffer with the directory in it."
  (interactive)
  (rename-buffer
   (concat "*"
           (car (last (butlast (split-string default-directory "/") 1)))
           "-shell*")))

;; Lisp configuration stuffs

;; M-. runs the command elisp-slime-nav-find-elisp-thing-at-point
;; M-, to navigate back
(require 'elisp-slime-nav)
(dolist (hook '(emacs-lisp-mode-hook ielm-mode-hook))
  (add-hook hook 'turn-on-elisp-slime-nav-mode))

;; SLIME and sbcl
(require 'slime)
(let ((sbcl-path "/usr/bin/sbcl")
      (slime-helper-path "~/quicklisp/slime-helper.el"))
  (when (and (file-exists-p sbcl-path)
	     (file-exists-p slime-helper-path))
    (load (expand-file-name slime-helper-path))
    (setq inferior-lisp-program sbcl-path)
    (setq slime-contribs '(slime-fancy))))

;;(require 'slime-repl-ansi-color)

;; move custom pkg dependency generated list out of here
(setq custom-file "~/.emacs.d/custom.el")
(write-region "" nil custom-file 'append)
(load custom-file)

(setq local-file "~/.emacs.d/local.el")
(write-region "" nil local-file 'append)
(load local-file)

(defun shell-clean-exec-last ()
  "Reset the shell buffer."
  (interactive)
  (delete-region (point-min) (point-max))
  (comint-previous-input 1)
  (comint-send-input)
  (fundamental-mode)
  (shell-mode)
  (compilation-shell-minor-mode))


(autoload 'ssh-config-mode "ssh-config-mode" t)
(add-to-list 'auto-mode-alist '("/\\.ssh/config\\'"     . ssh-config-mode))
(add-to-list 'auto-mode-alist '("/sshd?_config\\'"      . ssh-config-mode))
(add-to-list 'auto-mode-alist '("/known_hosts\\'"       . ssh-known-hosts-mode))
(add-to-list 'auto-mode-alist '("/authorized_keys2?\\'" . ssh-authorized-keys-mode))
(add-hook 'ssh-config-mode-hook 'turn-on-font-lock)

(require 'erc)
(setq erc-hide-list '("JOIN" "NICK" "PART" "QUIT" "MODE"))

(provide 'init)

;;; init.el ends here
