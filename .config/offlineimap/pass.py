from __future__ import print_function

import os
from subprocess import check_output

def get_pass(user):
    return check_output(
        [
            "gpg",
            "--use-agent",
            "--batch",
            "--no-tty",
            "--quiet",
            "--decrypt",
            os.path.join(
                os.environ["HOME"],
                ".passwords",
                "{}.gpg".format(user),
            ),
        ],
    ).strip()

if __name__ == "__main__":
    # Test that it works
    print(get_pass("augustin@augfab.fr"))
    print(get_pass("augustin.fabre@arm.com"))
